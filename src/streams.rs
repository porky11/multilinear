use super::{Channel, Event};

use data_stream::{FromStream, ToStream, from_stream, numbers::EndianSettings, to_stream};

use std::io::{Error, Read, Write};

impl<S: EndianSettings> ToStream<S> for Event {
    #[inline]
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.0, stream)
    }
}

impl<S: EndianSettings> FromStream<S> for Event {
    #[inline]
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self, Error> {
        Ok(Self(from_stream::<S, _, _>(stream)?))
    }
}

impl<S: EndianSettings> ToStream<S> for Channel {
    #[inline]
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<(), Error> {
        to_stream::<S, _, _>(&self.0, stream)
    }
}

impl<S: EndianSettings> FromStream<S> for Channel {
    #[inline]
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self, Error> {
        Ok(Self(from_stream::<S, _, _>(stream)?))
    }
}
