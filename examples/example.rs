use event_simulation::Simulation;
use multilinear::{Condition, EventInfo, MultilinearInfo, MultilinearSimulation};

fn main() {
    let mut story = MultilinearInfo::new();

    // Create state channels
    let place = story.add_channel();
    let clothes = story.add_channel();

    // Define event: Move from bedroom to living room
    let event_move = story.add_event(
        EventInfo::new()
            .with_change(&[Condition::change(place, 0, 1)])
            .unwrap(),
    );

    // Define event: Change clothes in bedroom
    let event_clothes = story.add_event(
        EventInfo::new()
            .with_change(&[Condition::new(place, 0), Condition::change(clothes, 1, 0)])
            .unwrap(),
    );

    let mut simulation = MultilinearSimulation::new(story);
    simulation.try_call(event_clothes);
    simulation.try_call(event_move);
    simulation.try_revert(event_move);
}
