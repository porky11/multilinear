# Multilinear Story System

[![Crates.io](https://img.shields.io/crates/v/multilinear)](https://crates.io/crates/multilinear)
[![Docs.rs](https://docs.rs/multilinear/badge.svg)](https://docs.rs/multilinear)

A Rust library for building interactive stories with constrained parallel state channels.

## Overview

Model narrative systems using:
- **Channels**: Independent state machines representing story aspects
- **Events**: Coordinated transitions across multiple channels
- **Safe Composition**: Guaranteed valid states through transition constraints

Inspired by petri nets but designed for narrative applications.

## Features

- 🚦 State channels with discrete value transitions
- ⚡ Event-driven story progression
- 🔄 Built-in undo/redo capabilities
- 🛡️ Prevention of invalid states during creation
- 📈 Efficient B-tree based state tracking

## Quick Start

```rust
use event_simulation::Simulation;
use multilinear::{Condition, EventInfo, MultilinearInfo, MultilinearSimulation};

let mut story = MultilinearInfo::new();

// Create state channels
let place = story.add_channel();
let clothes = story.add_channel();

// Define event: Move from bedroom to living room
let event_move = story.add_event(
    EventInfo::new()
        .with_change(&[Condition::change(place, 0, 1)])
        .unwrap(),
);

// Define event: Change clothes in bedroom
let event_clothes = story.add_event(
    EventInfo::new()
        .with_change(&[Condition::new(place, 0), Condition::change(clothes, 1, 0)])
        .unwrap(),
);

let mut simulation = MultilinearSimulation::new(story);
simulation.try_call(event_clothes);
simulation.try_call(event_move);
simulation.try_revert(event_move);
```

## Core Concepts

- Channel: Independent state machine representing a variable of the story state (like the player location or the relationship to somebody)
- Condition: A condition connected to a channel, also capable of changing the channel (like changing the player location from livingroom to kitchen)
- Event: Transition rule combining multiple channel conditions
